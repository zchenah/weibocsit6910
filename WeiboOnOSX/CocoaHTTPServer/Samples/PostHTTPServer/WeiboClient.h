//
//  WeiboClient.h
//  OSXClient
//
//  Created by Chen Zhao on 25/10/12.
//  Copyright (c) 2012 Chen Zhao. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Accounts/Accounts.h>
#import <Social/Social.h>

@interface WeiboClient : NSObject


-(NSString*)callAPIWithUrl:(NSString*)url andData:(NSDictionary*)postData;
- (NSString*)callAPIWithRetry:(NSString*)urlStr andData:(NSDictionary*)postData;


@property (retain) ACAccountStore* accountStore;
@property (retain) ACAccount* weiboAccount;
@property (retain) NSArray* weiboAccounts;

@property (atomic, retain) NSNumber* status;
@property (atomic, retain) NSString* content;
@property (atomic, retain) NSMutableDictionary* timeout;

@end
