#import "MyHTTPConnection.h"
#import "HTTPMessage.h"
#import "HTTPDataResponse.h"
#import "DDNumber.h"
#import "HTTPLogging.h"


#import "WeiboClient.h"

// Log levels : off, error, warn, info, verbose
// Other flags: trace
static const int httpLogLevel = HTTP_LOG_LEVEL_WARN; // | HTTP_LOG_FLAG_TRACE;


/**
 * All we have to do is override appropriate methods in HTTPConnection.
**/

@implementation MyHTTPConnection

- (BOOL)supportsMethod:(NSString *)method atPath:(NSString *)path
{
	return YES;
}

- (BOOL)expectsRequestBodyFromMethod:(NSString *)method atPath:(NSString *)path
{
	if([method isEqualToString:@"POST"])
		return YES;
	return [super expectsRequestBodyFromMethod:method atPath:path];
}

-(WeiboClient*)getNewClient
{
    return [[WeiboClient alloc] init];
}

-(WeiboClient*)staticClient
{
    static WeiboClient* client = nil;
    
    if (client==nil){
        client = [self getNewClient];
    }
    return client;
}

- (NSString*)handleWeiboAPICall:(NSString*)url withData:(NSString*)data
{
    NSString* apiUrl = [NSString stringWithFormat:@"https://api.weibo.com/2%@", url ];

    NSMutableDictionary *paras = [NSMutableDictionary dictionary];
    NSArray* kvs = [data componentsSeparatedByString:@"&"];
    
    for (NSString* kv in kvs) {
        if (kv.length==0)
            continue;
        NSArray* kvpair = [kv componentsSeparatedByString:@"="];
        paras[[kvpair objectAtIndex:0]] = [kvpair objectAtIndex:1];
    }
    NSString* r = [[self staticClient] callAPIWithRetry:apiUrl andData:paras];
    if (r.length<=100)
        return r;
    //some times the result has some garbage..
    NSRange head_garbage_edge = [r rangeOfString:@"{"];
    NSRange tail_garbage_edge = [r rangeOfString:@"}" options:NSBackwardsSearch];
    if (head_garbage_edge.location==NSNotFound || tail_garbage_edge.location==NSNotFound)
    {
        return r;
    }
    NSRange jsonRange;
    jsonRange.location = head_garbage_edge.location;
    jsonRange.length = tail_garbage_edge.location - head_garbage_edge.location +1;
    NSString* refined_r = [r substringWithRange:jsonRange];
    
    return refined_r;
}

- (NSObject<HTTPResponse> *)httpResponseForMethod:(NSString *)method URI:(NSString *)path
{
    if ([path isEqualToString:@"/favicon.ico"]){
        return [super httpResponseForMethod:method URI:path];
    }
    NSString *postStr = @"";
    NSData *postData = [request body];
    if (postData)
    {
        postStr = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
    }
    NSString* apiReturn = [self handleWeiboAPICall:path withData:postStr];
    NSData *response = [apiReturn dataUsingEncoding:NSUTF8StringEncoding];
    return [[HTTPDataResponse alloc] initWithData:response];
}

- (void)prepareForBodyWithSize:(UInt64)contentLength
{
	HTTPLogTrace();
	
	// If we supported large uploads,
	// we might use this method to create/open files, allocate memory, etc.
}

- (void)processBodyData:(NSData *)postDataChunk
{
	HTTPLogTrace();
	
	// Remember: In order to support LARGE POST uploads, the data is read in chunks.
	// This prevents a 50 MB upload from being stored in RAM.
	// The size of the chunks are limited by the POST_CHUNKSIZE definition.
	// Therefore, this method may be called multiple times for the same POST request.
	
	BOOL result = [request appendData:postDataChunk];
	if (!result)
	{
		HTTPLogError(@"%@[%p]: %@ - Couldn't append bytes!", THIS_FILE, self, THIS_METHOD);
	}
}

@end
