//
//  WeiboClient.m
//  OSXClient
//
//  Created by Chen Zhao on 25/10/12.
//  Copyright (c) 2012 Chen Zhao. All rights reserved.
//

#import "WeiboClient.h"

@implementation WeiboClient


- (id)init
{
    self = [super init];
    [self requestWeiboAuth];
    return self;
}


- (void)requestWeiboAuth
{
    self.weiboAccount=nil;
    self.accountStore = [[ACAccountStore alloc] init];
    ACAccountType *weiboAccountType = [self.accountStore
                                       accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierSinaWeibo];
    [self.accountStore requestAccessToAccountsWithType:weiboAccountType options:nil completion:^(BOOL granted, NSError *e) {
        if (granted) {
            self.weiboAccounts = [self.accountStore
                                 accountsWithAccountType:weiboAccountType];
            for (ACAccount* acc in self.weiboAccounts) {
                NSLog(@"auth: %@", acc.accountDescription);
            }
            self.weiboAccount = [self.weiboAccounts objectAtIndex:0];
        } else {
            NSLog(@"auth failed");
            // Fail gracefully...
        }
    }];
    while (self.weiboAccount==nil) {
        [NSThread sleepForTimeInterval:0.5];
    }
    return;
}

- (ACAccount*)getNextAccout
{
    static int cur = 0;
    cur+=1;
    if (cur>=self.weiboAccounts.count) {
        cur = cur%self.weiboAccounts.count;
    }
    return [self.weiboAccounts objectAtIndex:cur];
}

- (void) makeRequest:(SLRequest*)request AndKey:(NSString*)key
{//sometimes it returns 200 with empty content..
    request.account = [self getNextAccout];
    
    [request performRequestWithHandler:^(NSData *data,
                                         NSHTTPURLResponse *response,
                                         NSError *error) {
        if ([self.timeout objectForKey:key]!=nil ) {
            NSLog(@"timeout request returned. ignore");
            return ;
        }
        if (data==NULL){
            NSLog(@"null data returned, something wrong");
            self.content = @"";
            self.status = [NSNumber numberWithLong:[response statusCode]];
            return;
        }
        NSString* str = [NSString stringWithUTF8String:[data bytes]];
        
        if (str==nil)
            str = @"";
        NSLog(@"response status: %ld length: %ld", [response statusCode], str.length);
        if ([response statusCode]!=200 || str.length<10){
            NSLog(@"error. content: %@ account:%@", str, request.account.accountDescription);
        }
        self.content = str;
        self.status = [NSNumber numberWithLong:[response statusCode]];
    }];
}
- (NSString*)callAPIWithRetry:(NSString*)urlStr andData:(NSDictionary*)postData
{//sometime it returns empty string
    int max_try = self.weiboAccounts.count;
    int cur = 0;
    NSString* res;
    do {
        cur+=1;
        res = [self callAPIWithUrl:urlStr andData:postData];
        [NSThread sleepForTimeInterval:0.5];
    } while (res.length==0&&cur<max_try);
    return res;
}

- (NSString*)callAPIWithUrl:(NSString*)urlStr andData:(NSDictionary*)postData
{
    NSURL *requestURL = [NSURL URLWithString:urlStr];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeSinaWeibo
                                            requestMethod:SLRequestMethodGET
                                                      URL:requestURL
                                               parameters:postData];
    NSString* paraStr = [postData.description stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    paraStr = [paraStr stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString* key = [NSString stringWithFormat:@"%@ - %@", urlStr , paraStr];
    [self.timeout removeObjectForKey:key];
    NSLog(@"makeRequest: %@ %@...", urlStr, paraStr);
    [self makeRequest:request AndKey:key];

    int wait_times = 0;
    while (self.status==nil) {
        [NSThread sleepForTimeInterval:0.5];
        wait_times++;
        if (wait_times>10){
            [self.timeout setObject:@"timeout" forKey:key];
            NSLog(@"SLRequest timeout: %@", key);
            
            self.content = @"timeout";
            break;
        }
    }
    self.status = nil;
    return self.content;
}


@end
