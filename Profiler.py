#!/usr/bin/env python
# -*- encoding:utf-8 -*-
'''
Created on Oct 24, 2012

@author: chin
'''
import redis
from collections import deque, defaultdict
from sets import Set

import networkx as nx
from networkx.readwrite import json_graph
from networkx.readwrite import gexf
import os
import os.path
import math

import logconfig
import logging
import logging.config
logging.config.dictConfig(logconfig.LOGGING)
log = logging.getLogger(__name__)

from WeiboModel.models import * #@UnusedWildImport

CACHE_PATH=os.path.join(os.path.dirname(__file__))

def unshared_copy(inList):
    if isinstance(inList, list):
        return list( map(unshared_copy, inList) )
    return inList

edge_counter = 0
friends_loaded_counter = 0
class Grapher(object):
    @staticmethod
    def add_friends_to_g(g, u, level):
        global edge_counter
        global friends_loaded_counter
        #depth first is a nightmare
        if level>0:
            friends = u.bilateral_friends()
            friends_loaded_counter+=1
            if friends_loaded_counter%100==0:
                log.info("friends loaded %d edge added: %d"%(friends_loaded_counter, edge_counter))
            for f in friends:
                edge_counter +=1
                g.add_edge(u.name(), f.name())
                Grapher.add_friends_to_g(g, f, level-1)
    @staticmethod
    def add_friends_to_g_width_first(g, user, level):
        chain = []; queue = deque([chain, user])
        edge_counter = 0; friends_loaded_counter = 0; process_counter = 0
        while len(queue)>0:
            cur = queue.popleft()
            if type(cur)==list:
                process_counter = 0
                chain = cur
                cur = queue.popleft()
                chain.append([])
            friends = cur.friends(); follows = cur.follows() #just get the cache
            friends_loaded_counter+=1
            for f in friends:
                edge_counter+=1
                g.add_edge(cur.name(), f.name())
            process_counter+=1
            if len(chain)>1:
                chain[-2][1] = process_counter
            chain[-1] = [cur.name(), 0, len(friends)]#cur.bi_followers_count()]
            print "%4d %6d"%(friends_loaded_counter, edge_counter), "->".join("%s[%s]"%(u[0], "/".join(str(s) for s in u[1:])) for u in chain)
            if len(chain)<=level:
                queue.append(unshared_copy(chain))
                queue.extend(friends)
                
    @staticmethod
    def get_raw_graph(uid, level):
        fname_for_visual = os.path.join(CACHE_PATH, "raw-graph-%s-%s.gexf"%(str(uid), str(level)))
        fname_pickle = os.path.join(CACHE_PATH, "raw-graph-%s-%s.pickle"%(str(uid), str(level)))
#        if os.path.exists(fname_pickle):
#            g = nx.read_gpickle(fname_pickle)
#            print "graph loaded from %s"%(fname_pickle)
#            return g
        u = WeiboUser.fromUid(uid)
        g=nx.Graph()
        Grapher.add_friends_to_g_width_first(g, u, level)
        nx.write_gpickle(g, fname_pickle)
        #gexf.write_gexf(g, fname_for_visual)
        return g
    
temp_rdb = redis.StrictRedis(host='localhost', port=7777, db=0)
class Processor(object):
    def __init__(self):
        pass
    def like(self, c, d):
        if c==d:
            return False
#        if len(c)>2*len(d) or len(c)<len(d)/2 or (len(c)<5 and len(d)<5):
#            return False
        large = max(len(c), len(d))
        small = min(len(c), len(d))
        both = len(set(c) & set(d))
        return both>large/2 or both>=4*small/5
    
    def diffuse_leaders(self, leader_uids, decay=0.1, full_index = 1, cache = True):
        key = "|".join(str(uid) for uid in leader_uids)
        leaders = [WeiboUser.fromUid(uid) for uid in leader_uids]
        map(lambda u:u.setIndex(full_index), leaders)
        result = Set()
        result.update(leaders)
        if cache and temp_rdb.exists(key):
            return [WeiboUser.fromUid(uid).setIndex(float(index)) for uid, index in temp_rdb.hgetall(key).iteritems()]

        def diffuse_index_to_friends(user, decay):
            friends = Set()
            for u in user.friends():
                old_index = u.getIndex()
                new_index = old_index + (full_index-old_index)*decay
                u.setIndex(new_index)
                friends.add(u)
            return friends

        leader_friends = Set()
        for leader in leaders:#each friends of leaders is possible another leader
            leader_friends.update(diffuse_index_to_friends(leader, decay))
        result.update(leader_friends)

        leader_friends_friends = Set()
        for leader_friend in leader_friends:#friends' friends
            leader_friends_friends.update(diffuse_index_to_friends(leader_friend, decay/2))#very important to choose the decay
        result.update(leader_friends_friends)

        for leader in result:
            temp_rdb.hset(key, leader.uid, leader.getIndex())
        print 'diffusing done'
        return result
    
    def static_index(self, users):
        d = defaultdict(set)
        for u in users:
            d[u.getIndex()].add(u)
        f = file('IT.txt', 'w')
        for k,v in sorted(d.items(), key=lambda x:x[0], reverse = True ):
            for u in v:
                f.write(('%s %s %s\n'%(u.uid, u.name(), str(k))).encode('utf-8'))
            print k,len(v),
            if len(v)<500:
                print ','.join(u.name() for u in v)
            else:
                print
        f.close()
        print 'total', len(users)
        
    def index_from_friends(self, user, leaders):
        if_cache = True
        index = 0
        friends = user.friends(cache=if_cache)
        friend_base = min(len(friends), 100)
        friend_leader_count = 0
        #print 'all friends:', ",".join(f.name() for f in friends)
        #print 'industry friends:',
        for friend in friends:
            if friend in leaders:
                friend_leader_count+=1
                #print "%s %.3f"%(friend, friend.getIndex()),
                index += friend.getIndex()/friend_base
                friend_base = (friend_base-10)/2 #+2 to avoid 0
                if friend_base<1:friend_base=1
        if friend_leader_count>10:
            print user, 'friends: %d/%d  %f'%( friend_leader_count, len(friends), index )
            pass
        return min(index,1)
    def index_from_follows(self, user, leaders):
        if_cache = True
        index = 0
        follows = user.follows(cache=if_cache)
        follow_base = min(len(follows),200)
        follow_leader_count = 0
        #print '----------------------------------------------------------'
        #print '\n--------------------'
        #print 'all follows:', ",".join(f.name() for f in follows)
        #print 'industry follows:',
        for follow in follows:
            if follow in leaders:# and follow not in friends: #friends just cal twice
                #print "%s %.3f"%(follow, follow.getIndex()),
                follow_leader_count+=1
                index += follow.getIndex()/follow_base
                follow_base = (follow_base-10)/1.1
                if follow_base<1:follow_base=1
        if follow_leader_count>10:
            print user, 'follows: %d/%d  %f'%( follow_leader_count, len(follows), index )
        return min(index,1)
    def index_from_group(self, user, leaders):
        friends  = user.friends()
        index_dict = {}
        for f in friends:
            index_dict[f] = max(self.index_from_friends(f, leaders) , self.index_from_follows(f, leaders))
        group_base = min(20, len(friends)/20)
        index = sum(sorted(index_dict.values(), reverse=True)[:group_base])/group_base
        return index


    def industry_index(self, uids, leader_uids):
        print 'industry_index begin'
        after_diffusing_leaders = self.diffuse_leaders(leader_uids, cache=True)
        #self.static_index(after_diffusing_leaders)
        #return 
        good_leaders = set(leader for leader in after_diffusing_leaders if leader.getIndex()>0.9)
        print 'leaders: %d'%len(good_leaders)
        for uid in uids:
            user = WeiboUser.fromUid(uid)
            from_friend = self.index_from_friends(user, good_leaders)
            from_follow = self.index_from_follows(user, good_leaders)
            from_group = self.index_from_group(user, good_leaders)
            
            index = from_friend + from_follow + from_group
            user.setIndex(index)
            print "\n*** %s's index: %f = %f(friend) + %f(follow) + %f(group) ***"%(user, index, from_friend , from_follow, from_group)
            print '----------------------------------------------------------'
    def subgraph_friends(self, uid):
        user = WeiboUser.fromUid(uid)
        level = 1
        min_cliques = 3
        raw_g = Grapher.get_raw_graph(uid, level)
        print "edges:%d  nodes:%d"%( len(raw_g.edges()), len(raw_g.nodes()))
        #cliques = nx.k_clique_communities(raw_g, min_cliques)
        cliques =  list(l for l in nx.find_cliques(raw_g) if len(l)>=min_cliques)
        my_cliques = [c for c in cliques if user.name() in c]
        
        goon = True
        while(goon):
            goon = False
            for c in my_cliques:
                for d in my_cliques:
                    if self.like(c,d):
                        goon = True
                        for p in d:
                            if p not in c:
                                c.append(p)
                        my_cliques.remove(d)
                    
        for n,c in enumerate(my_cliques):
            print n, ",".join(c)
        print "edges:%d  nodes:%d"%( len(raw_g.edges()), len(raw_g.nodes()))
    pass
        
#1804089927 zz
#2023773473 me
#1693300644 wangheng
#1770065200 huangfuyang
#1979929707 zengzhiping
#1671443481 xiongjun
#1082251714 yiyu

#1701886454 xushiwei
#1924010407 laiyonghao
#1882579600 liuweipeng
#1747724431 miloyip
#2388714105 yunfeng
####
#1560442584 laozhao
#1400936805 zoomquite
#10503 timyang
#1772882767 zhenghui
#1655038093 yongsun

#1701018393 chenshuo
#1729408273 liuxin
#1400229064 tinyfool
#1645518723 chenhuailin
#1401880315 chenhao

def test():
    p = Processor()
    uid = 2023773473
    #p.subgraph_friends(uid)
    coders = [ 1729408273 , 1400229064 , 1645518723 , 1401880315,1701018393,
               1655038093,1772882767,10503,1400936805,1560442584,
              2388714105, 1747724431,1882579600, 1924010407,1701886454, ]
    cars = [1644088831, 1939419914, 1646601370, 1761731257, 1898599615, 2638846080, 1731930915, 2598523295,
            2624870984, 1645817972, 1901060075, 1891845373, 1828685865, 1270873141, 1784571423,1659553111]
    doctors = [1228977142, 1893826915, 1407899537, 2564684277, 1951704207, 1991620070, 1729109650,1283658605,1445105811,1658988854, 1280981624,1598061257, 1848322945,1307233960,1146222447,2623922575,2258386671,1854129535,2200539403,1901679801,1065203750,1827305564]
    doctors = [ 1658988854, 1280981624,1598061257, 1848322945,1307233960,1146222447,2623922575,2258386671,1854129535,2200539403,1901679801,1065203750,1827305564]
    crawler = Crawler()
    for c in doctors:
        crawler.getFriendsOfFriends(c)
    return
        
    to_test = [1082251714 , 1785553917, 2474296873]
    to_test = [ 1849049573, 1804089927, 2023773473 , 1693300644 , 1770065200 , 1979929707, 1671443481, 1082251714,1785553917,2011597062,2261955015,2539149125,2474296873, 1821356925,1185648102, 1072144721,1986364205, 1867300865, 2151022894,1529636654, 1658405853]
    to_test = [2876120104,1764038970]
    #print p.industry_index(to_test, cars[:1])
    print p.industry_index(to_test, coders)
    #rdb.save()
    
if __name__=='__main__':
    test()
    print "over"
