#!/usr/bin/env python
# -*- encoding:utf-8 -*-
import logging
import json
import time
import datetime
import urllib
import urllib2
import weibo

log = logging.getLogger(__name__)


class AuthServerInfoSource(object):
    def __init__(self):
        self.host = "http://csz908.cse.ust.hk/auth"
        self.get_token_url    = self.host+"/token/get"
        self.limit_token_url  = self.host+"/token/limit"
        self.expire_token_url = self.host+"/token/expire"
    def expireLastToken(self):
        urllib2.urlopen(self.expire_token_url+"?access_token="+self.token).read()
    def limitLastToken(self):
        urllib2.urlopen(self.limit_token_url+"?access_token="+self.token).read()
    def getToken(self):
        try_time = 0
        while try_time<1:
            try:
                log.info("getting token from %s"%(self.get_token_url,))
                token_jsn_str = urllib2.urlopen(self.get_token_url).read()
                log.info("got token: %s"%(token_jsn_str,))
                self.token = json.loads(token_jsn_str)['access_token']
                return self.token
            except Exception,e:
                log.error("getting token error: %s",str(e))
                try_time+=1
        raise Exception("token server error")
        
def invalidJson(jsonStr):
    #jsonStr = jsonStr.replace("\",\"", "\",\n\"")
    try:
        j = json.loads(jsonStr)
        return j.has_key("error") or j.has_key("error_code")
    except Exception, e:
        #log.error(jsonStr)
        log.error("json error: %s", e)
        return True
    pass

class MixedClient(object):
    def __init__(self, return_json=False):
        self.client = OSXClient()
        self.backup_client = OSXClient()#NoAuthAPIClient()
        self.return_json = return_json
    def __getattr__(self, attr):
        def wrap(**kw):
            r = getattr(self.client, attr)(**kw)
            if invalidJson(r):
                log.warn("invalid json from OSXClient: %s %s", attr, kw)
                r = getattr(self.backup_client, attr)(**kw)
            if self.return_json:
                return json.loads(r)
            else:
                return r
        return wrap    
    
class OSXClient(object):
    @staticmethod
    def encode_params(**kw):
        args = []
        for k, v in kw.iteritems():
            qv = v.encode('utf-8') if isinstance(v, unicode) else str(v)
            args.append('%s=%s' % (k, urllib.quote(qv)))
        return '&'.join(args)

    def __getattr__(self, attr):
        def wrap(**kw):
            try:
                return self.callOSX(attr, kw)
            except weibo.APIError,e:
                log.info("api error: %s", e.error)
                raise e
        return wrap
    def callOSX(self, path, para):
        url = "http://localhost:8888/%s.json"%(path.replace('__', '/'))
        data = OSXClient.encode_params(**para)
        r = urllib2.urlopen(url, data).read()
        r = self.try_fix_invalid_json(r)
        log.debug("OSXClient: %s %s", path, repr(para))
        return r
    def try_fix_invalid_json(self,jsonStr):
        try: #       Extra data: line 1 column 56053 - line 1 column 61582
            j = json.loads(jsonStr)
        except Exception, e:
            if "Extra data:" in e.message:
                return jsonStr[:int(e.message.split()[5])]
        return jsonStr
    
class NoAuthAPIClient(object):
    def __init__(self, token_src=AuthServerInfoSource(), return_json=False):
        self.return_json=return_json
        self.token_src = token_src
        self.weibo_client = None
    def refresh(self):
        self.token = self.token_src.getToken()
        self.weibo_client = weibo.APIClient(self.token)
    def callWeiboAPI(self, attr, **kw):
        if not self.weibo_client:
            self.refresh()
        respStr = getattr(self.weibo_client, attr)(**kw)
        if self.return_json:
            return json.loads(respStr)
        return respStr
    def __getattr__(self, attr):
        def wrap(**kw):
            try:
                return self.callWeiboAPI(attr, **kw)
            except weibo.APIError,e:
                log.info("token error: %s", e.error)
                if "limit" in e.error or 'unaudited' in e.error:
                    self.token_src.limitLastToken()
                elif "expire" in e.error:
                    self.token_src.expireLastToken()
                self.weibo_client = None
                return wrap(**kw)
        return wrap

def test_inferface():
    c = NoAuthAPIClient()
    print c.statuses__user_timeline(count=3)
    c = MixedClient()
    print c.statuses__user_timeline(count=3)
    #print c.users__show(uid=1804089927)
    "https://api.weibo.com/2/statuses/friends_timeline.json"
if __name__=='__main__':
    test_inferface()
