#!/usr/bin/env python
#-*- encoding:utf-8 -*-
'''
Created on Oct 16, 2012

@author: chin
'''
import logging
import datetime as dt
import json
import redis
import leveldb
import Queue
import threading
import sys
import os.path

from WeiboClient.clients import MixedClient, NoAuthAPIClient

log = logging.getLogger(__name__)

client = MixedClient(return_json = True)

PATH = os.path.dirname(os.path.abspath(__file__))
rdb = redis.StrictRedis(host='localhost', port=6379, db=0)
ldb = leveldb.LevelDB(os.path.join(PATH,'user.leveldb'))

class Crawler(object):
    def __init__(self):
        self.friendsQueue = Queue.Queue()
        self.threads = []
        for i in range(15):
            t = threading.Thread(target=self.worker)
            self.threads.append(t)
        for t in self.threads:
            t.start()
    def getFriendsOfFriends(self, uid):
        self.user = WeiboUser.fromUid(uid)
        friends = self.user.friends()
        print "getFriendsOfFriends", self.user.name(), "friends:", len(friends)
        for f in friends:
            self.friendsQueue.put(f)
        self.friendsQueue.join()
        print 'getFriendsOfFriends over'
    def worker(self):
        #client = NoAuthAPIClient(return_json = True)
        client = MixedClient(return_json = True)
        while True:
            u = self.friendsQueue.get()
            u.setClient(client)
            try:
                u.friends()
            except Exception,e:
                log.error("something wrong%s", e.message)
                print e
            self.friendsQueue.task_done()
            print threading.current_thread(), self.user.name(), self.friendsQueue.qsize()
        try:
            pass
        except Queue.Empty,e:
            print threading.current_thread(), self.user.name(), 'done'
            return
    pass

WeiboUserDict = {}
class WeiboUser(object):
    @staticmethod
    def fromUid(uid):
        uid = str(uid)
        if uid in WeiboUserDict:
            return WeiboUserDict[uid]
        u = WeiboUser()
        u.uid = uid
        #WeiboUserDict[uid] = u
        return u
    @staticmethod
    def save_user(user_json):
        user_json["last_update"] = str(dt.datetime.now().isoformat(' '))
        #rdb.hset('user', user_json['id'], json.dumps(user_json))
        ldb.Put(str(user_json['id']), json.dumps(user_json))
    @staticmethod
    def fromJson(json):#only those just arrived data need call this, so try save 
        u = WeiboUser.fromUid(json["id"])
        u.json_profile = json
        WeiboUser.save_user(json)
        return u
    def __init__(self, **kargs):
        self.uid = kargs.get('uid', None)
        self.json_profile=None
        self.client = client
    def __str__(self):
        s = u'%s|%s'%(self.name(), self.uid)
        return s.encode('utf-8')#this is weird because if when print user directly, python use ascii..
    #def __repr__(self):
        #return str(self)
    def setClient(self, client):
        self.client = client
    def follows(self, cache =True):
        if cache and rdb.hexists('follow', self.uid):
            lst = [WeiboUser.fromUid(uid) for uid in json.loads(rdb.hget('follow', self.uid))]
            log.debug("follow of %s in db. len:%d"%(self.name(), len(lst)))
            return lst
        page_size = 200
        page = 1
        jsnRslt = self.client.friendships__friends(uid=self.uid, count=str(page_size), page=str(page))
        users = jsnRslt["users"]
        total = jsnRslt["total_number"]
        while total>page_size and len(users)<total*0.9 and page<20:
            page+=1
            jsnRslt = self.client.friendships__friends(uid=self.uid, count=str(page_size), page=str(page))
            users.extend(jsnRslt["users"])
        follows = [WeiboUser.fromJson(user_json) for user_json in users]
        follows_uid_list = [u.uid for u in follows]
        rdb.hset('follow', self.uid, json.dumps(follows_uid_list))
        log.debug("follow of %s save to db. len:%d"%(self.name(), len(follows_uid_list)))
        return follows
        
    def friends(self, cache=True):
        if cache and rdb.hexists('friend', self.uid):
            lst = [WeiboUser.fromUid(uid) for uid in json.loads(rdb.hget('friend', self.uid))]
            db_count = len(lst)
            log.debug("friend of %s in db. %d/%d"%(self, db_count, self.friends_count()))
            #if not (((db_count==0 and db_count!=self.friends_count()) or db_count<self.friends_count()/2) ) : #magic number 51
            if db_count!=0:# and (db_count+20>self.friends_count() or db_count>self.friends_count()/2):
                return lst
        page_size = 200
        users = []
        for page in range(1, self.friends_count()/page_size + 2):
            jsnRslt = self.client.friendships__friends__bilateral(uid=self.uid, count=str(page_size), page=str(page))
            users.extend(jsnRslt["users"])
        #total = jsnRslt["total_number"] #this is the real total number
        friends = [WeiboUser.fromJson(user_json) for user_json in users]
        rdb.hset('friend', self.uid, json.dumps([u.uid for u in friends]))
        log.debug("friend of %s save to db. %d/%d"%(self, len(friends), self.friends_count()))
        return friends
    def setIndex(self, v):
        self.index = v
        return self
    def getIndex(self, v=0):
        if not hasattr(self,'index'):
            self.index = v
        return self.index 
    def getJson(self):
        try:
            self.json_profile = json.loads(ldb.Get(self.uid))
        except KeyError,e:
            self.json_profile = client.users__show(uid=self.uid)
            WeiboUser.save_user(self.json_profile)
            """
        if rdb.hexists('user', self.uid):
            self.json_profile = json.loads(rdb.hget('user', self.uid))
        else:
            self.json_profile = client.users__show(uid=self.uid)
            WeiboUser.save_user(self.json_profile)
            """
    def location(self):
        if not self.json_profile:
            self.getJson()
        return self.json_profile["location"]
        pass
    def name(self):
        if not self.json_profile:
            self.getJson()
        return self.json_profile["name"]
    def follows_count(self):
        if not self.json_profile:
            self.getJson()
        return self.json_profile["friends_count"]
    def friends_count(self):#the same as above
        if not self.json_profile:
            self.getJson()
        return self.json_profile["bi_followers_count"]


def main():
    uid = 1656063147
    user = WeiboUser.fromUid(uid)
    print "%s"%(user,)
    print user
    locations = [f.location() for f in user.friends()]
    for l in locations:
        print l
    pass

if __name__=='__main__':
    main()

