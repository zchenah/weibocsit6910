import os.path
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(asctime)s %(thread)d %(levelname)s %(filename)s %(lineno)d %(module)s.%(funcName)s: %(message)s'
        },
        'normal': {
            'format': '%(asctime)s %(levelname)s %(message)s'
        },
    },
    'filters': {
    },
    'handlers': {
        'null': {
            'level':'DEBUG',
            'class':'django.utils.log.NullHandler',
        },
        'console':{
            'level':'INFO',
            'class':'logging.StreamHandler',
            'formatter': 'normal'
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
          },  
        'file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'verbose',
            'filename': os.path.join(os.path.dirname(__file__),'all.log'),
            'maxBytes': 20480000,
            'backupCount': 3, 
         }
    },
    'loggers': {
        'django': {
            'handlers':['console','null'],
            'propagate': True,
            'level':'INFO',
        },
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        '': {
            'handlers': ['file','console'], # 
            'propagate': True,
            'level': 'DEBUG',
        },
    },
   'root': {
        'handlers': ['console', 'file'],
        'level': 'DEBUG'
    }
}
