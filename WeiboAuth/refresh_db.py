#!/usr/bin/env python
#author: Chen Zhao
import sys
import os
import os.path
import subprocess
self_path = os.path.split(__file__)[0]
sys.path.append(self_path)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "WeiboAuth.settings")
import WeiboAuth.settings as settings
import WeiboAuth.authenticator.views
from WeiboAuth.authenticator.models import *


def insert_users(user_list):
    r = []
    for name,pwd in user_list:
        user = TestingUser(name=name, pwd=pwd)
        user.save()
        r.append(user)
    return r

def insert_app_with_users(appinfo_list, usersinfo_list):
    app = WeiboApp(name=appinfo_list[0],
            key=appinfo_list[1],
            secret=appinfo_list[2],
            redirect_uri=appinfo_list[3],
            info=appinfo_list[4],
            )
    app.save()
    users = insert_users(usersinfo_list)
    for user in users:
        app.testing_users.add(user)
    app.save()

def init_data():
    app = ("one_swarm", "2792767666",
            "8db5b929aada4a3f7b2f8d07059a6a4a",
            "http://www.google.com", "client")
    users = [
        ("cnjswangheng66@yahoo.com.cn",     "swarmhere"),
        ("stevecreateswarm@gmail.com",     "swarmhere"),
        ("swarmben@126.com",     "swarmhere"),
        ("swarmbenben@126.com",     "swarmhere"),
        ("swarmweibo@126.com",     "swarmhere"),
        ("swarmheng@126.com",     "swarmhere"), ]
    insert_app_with_users(app, users)
####################################################
    app = ("two_swarm", "3050245731",
            "9c84a9381d24a789c31e363dd07b428e",
            "http://www.google.com", "client",)
    users = [
        ("swarmben@yeah.net",     "swarmhere"),
        ("swarmbenben@yeah.net",  "swarmhere"),
        ("swarmweibo@yeah.net",   "swarmhere"),
        ("swarmheng@yeah.net",    "swarmhere"),
        ("swarmben@163.com",      "swarmhere"),
        ("swarmbenben@163.com",   "swarmhere"),]
    insert_app_with_users(app, users)
####################################################
    app=("three_swarm", "1594084576",
        "6468603eb7763ff11066dbbd168de9a7",
        "http://www.google.com", "web")
    users = [
        ("dbtest001@126.com",        "swarmhere"),
        ("dbtest002@126.com",       "swarmhere"),
        ("dbtest003@126.com",        "swarmhere"),
        ("dbtest004@126.com",        "swarmhere"),
        ("project6910@gmail.com",    "swarmhere"),
        ("project6910@sohu.com",     "swarmhere"),]
    insert_app_with_users(app, users)
####################################################
    app=("four_swarm", "1977875290",
        "aa7f1037881efd2d14572af9b1c83c74",
        "http://www.google.com", "web")
    users = [
        ("project6910@126.com",          "swarmhere"),
        ("project6910@mysinamail.com",   "swarmhere"),
        ("zchenah0@gmail.com",           "iloveust"),
        ("zchenah1@gmail.com",           "iloveust"),
        ("zchenah@gmail.com",            "iloveust"),
        ("zchenah@ust.hk",               "iloveust"),]
    insert_app_with_users(app, users)


def main():
    #os.remove("./WeiboAuth/"+settings['default']['name'])
    manage_path = os.path.join(self_path, 'manage.py')
    cmd = "python %s sqlclear authenticator | python %s dbshell"%(manage_path, manage_path)
    print cmd
    subprocess.call(cmd, shell=True)
    subprocess.call("python %s syncdb"%(manage_path,), shell=True)
    init_data()
    pass

if __name__=='__main__':
    main()

