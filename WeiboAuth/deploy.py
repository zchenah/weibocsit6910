#!/usr/bin/env python
#author: Chen Zhao
from fabric.api import env
from fabric.operations import put, sudo, run, local
from fabric.contrib.project import rsync_project

env.hosts = ["swarm@csz908.cse.ust.hk:22"]
env.password = "swarm"

env.host_string = env.hosts[0]

DEST = "/home/swarm/auth_django_deploy_temp/" 
DEST = "/var/www/auth/" 
def upload():
    rsync_project(remote_dir=DEST, local_dir=".", exclude=["*.log","*.sqlite","*.pyc"])
    sudo("python /var/www/auth/manage.py collectstatic", shell=True)
    sudo("chmod 777 /var/www/auth/WeiboAuth/all.log", True)
    sudo("chmod 777 /var/www/auth/WeiboAuth/WeiboAuth.sqlite", True)
    sudo("chmod 777 /var/www/auth/WeiboAuth/", True)
    sudo("apache2ctl restart", True)
    #run("rm -r ~/auth_django_deploy_temp/")

def main():
    upload()
    pass

if __name__=='__main__':
    main()

