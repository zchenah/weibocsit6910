#!/usr/bin/env python
import sys
import time
from pyvirtualdisplay import Display

from selenium import webdriver


def main(redirect_uri, client_id, username, password):
    display = Display(visible=0, size=(800, 600))
    display.start()
    auth_url = "https://api.weibo.com/oauth2/authorize?redirect_uri=%s&response_type=code&client_id=%s&display=mobile" % (redirect_uri, client_id)

    browser = webdriver.Firefox()
    browser.get(auth_url)
    browser.find_element_by_id("userId").send_keys(username)
    browser.find_element_by_name("passwd").send_keys(password)
    browser.find_element_by_class_name("btnP").click()
    
    CODE_TIMEOUT = 60
    wait_seconds = 0
    step2_auth_url = "https://api.weibo.com/oauth2/authorize"
    while not "code=" in browser.current_url:
        time.sleep(1)
        wait_seconds+=1
        if browser.current_url==step2_auth_url:
            browser.find_element_by_class_name("btnP").click()
        if wait_seconds>CODE_TIMEOUT:
            browser.close()
            sys.exit("timeout!")
    print browser.current_url
    browser.close()
    display.stop()

if __name__=='__main__':
    if len(sys.argv)!=5:
        print "invalid args, do nothing"
        sys.exit()
    main(*sys.argv[1:])
