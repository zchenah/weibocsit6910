from django.db import models
# Create your models here.
class TestingUser(models.Model):
    name = models.CharField(max_length=200)
    pwd = models.CharField(max_length=200)
    created_on = models.DateTimeField(auto_now_add=True)
    last_modified_on = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return self.name

class WeiboApp(models.Model):
    key = models.CharField(max_length=200)
    secret = models.CharField(max_length=200)
    redirect_uri = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    info = models.CharField(max_length=200)
    testing_users = models.ManyToManyField(TestingUser)
    created_on = models.DateTimeField(auto_now_add=True)
    last_modified_on = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return self.name

class AccessToken(models.Model):
    user = models.ForeignKey(TestingUser)
    app = models.ForeignKey(WeiboApp)
    access_token = models.CharField(max_length=200)
    expires_in = models.CharField(max_length=200, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    first_used_on = models.DateTimeField(null=True)
    expired_on = models.DateTimeField(null=True)
    limited_on = models.DateTimeField(null=True)
    def to_dict(self):
        return {"access_token":self.access_token}
    @staticmethod
    def insert(tokenJsn, app, user):
        token = AccessToken(user=user, 
                            app=app, 
                            access_token=tokenJsn["access_token"],
                            expires_in=tokenJsn["expires_in"])
        token.save()
        return token
    def __unicode__(self):
        return self.access_token
        return "%s %s %s"%(self.app.name, self.user.name, self.access_token)
