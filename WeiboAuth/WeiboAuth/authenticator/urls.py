#!/usr/bin/env python
#author: Chen Zhao
from django.conf.urls import patterns, include, url

urlpatterns = patterns('WeiboAuth.authenticator',
        url(r'^get$', 'views.get'),
        url(r'^limit$', 'views.limit'),
        url(r'^expire$', 'views.expire'),
        url(r'^exclusive$', 'views.exclusive'),
        )
