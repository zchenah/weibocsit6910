# -*-encoding:utf8 -*-
import logging
import json
from django.http import HttpResponse
from models import *
from django.db.models import Q
import datetime as dt
import TokenGenerator

log = logging.getLogger(__name__)
#if a token has been limited for 1 hour, retry using it
TOKEN_LIMIT_TIME = dt.timedelta(hours=1) 
#if a token has been using for 1 day, 
#    it should alreay expired, recycle the app,user pair
TOKEN_RECYCLE_TIME = dt.timedelta(days=1)

def expireEarlyTokens():
    expired_tokens = AccessToken.objects\
            .filter(first_used_on__lt=dt.datetime.now()-TOKEN_RECYCLE_TIME)
    for token in expired_tokens:
        token.expired_on = dt.datetime.now()
        token.save()
def getFreeAppUserPair():
    expireEarlyTokens()
    using_tokens = AccessToken.objects\
            .filter(first_used_on__gt=dt.datetime.now()-TOKEN_RECYCLE_TIME)\
            .filter(expired_on=None)
    using_users = [t.user for t in using_tokens]
    for app in WeiboApp.objects.all():
        for user in app.testing_users.all():
            if user not in using_users:
                return app, user
    return None,None

def getOldestToken():#todo, useful?
    tokens = AccessToken.objects\
                .filter(expired_on=None).order_by('first_used_on')
    log.debug("getOldestTokens: get tokens %d"%(len(tokens)))
    if len(tokens)>0:
        return tokens[0]

def getOldestAppUserPair():
    tokens = AccessToken.objects\
                .filter(expired_on=None).order_by('first_used_on')
    log.debug("getOldestAppUserPair: get tokens %d"%(len(tokens)))
    for token in tokens:
        for app in WeiboApp.objects.all():
            #log.debug("getOldestAppUserPair: get app %s"%(app.name))
            for user in app.testing_users.all():
                #log.debug("getOldestAppUserPair: get user %s  and compair with %s"%(user.name, token.user.name))
                if user.name == token.user.name:
                    return app, user

def isValidTokenJsn(tokenJsn):
    try:
        return tokenJsn.has_key("access_token")
    except Exception,e:
        log.error("invalid token json:%s", str(tokenJsn))
        return False

def genToken(app, user):
    l = (app.key, app.secret, app.redirect_uri, user.name, user.pwd)
    tokenJsn = TokenGenerator.getTokenJsn(*l)
    log.info("generated a new token: %s", tokenJsn)
    
    if tokenJsn and isValidTokenJsn(tokenJsn):
        token = AccessToken.insert(tokenJsn, app, user)
        return token
    else:
        token = AccessToken.insert({"access_token":"fake token", "expires_in":"0", }, app, user)# to info the app and user is locked, need refactor
        token.first_used_on = dt.datetime.now()
        token.limited_on = dt.datetime.now()+TOKEN_RECYCLE_TIME
        token.save()
        return None


def getLimitOverTokens(): 
    tokens = AccessToken.objects.filter(expired_on=None)\
                .filter(limited_on__lt=dt.datetime.now()-TOKEN_LIMIT_TIME)
    return tokens

def getTotallyUnusedTokens(): #you wish that, actually this should never happen
    unused_tokens = AccessToken.objects.filter(expired_on=None)\
                                        .filter(limited_on=None)\
                                        .filter(first_used_on=None)
    return unused_tokens

def setFirstUsedTime(token):
    token.first_used_on = dt.datetime.now()
    token.save()
def setNoLimit(token):
    token.limited_on = None
    token.save()

def getValidToken(): #complex policy, very bad
    token = None
    log.info("getValidToken: begin")
    while token==None:
        unused_tokens = getTotallyUnusedTokens()
        if unused_tokens:
            token = unused_tokens[0]
            if token:
                setFirstUsedTime(token)
                log.info("get unused token: %s", repr(token))
        else: #free ones first
            free_app, free_user = getFreeAppUserPair()
            if free_app and free_user:
                token = genToken(free_app, free_user)
                if token:
                    setFirstUsedTime(token)
                    log.info("gen token with free app,user: %s", repr(token))
            else: #limite over ones then
                limit_over_tokens = getLimitOverTokens()
                if limit_over_tokens:
                    token = limit_over_tokens[0]
                    if token:
                        setNoLimit(token)
                        log.info("get limit over token: %s", repr(token))
                else: #currently the first used one
                    app, user = getOldestAppUserPair()
                    token = genToken(app, user)
                    if token:
                        setFirstUsedTime(token)
                        log.info("gen token with oldest app,user: %s", repr(token))
    return token 

def setLimit(tokenStr):
    tokens = AccessToken.objects.filter(access_token=tokenStr)
    for token in tokens:
        if token.limited_on==None:
            token.limited_on = dt.datetime.now()
            token.save()

def setExpire(tokenStr):#useful?
    tokens = AccessToken.objects.filter(access_token=tokenStr)
    for token in tokens:
        if token.expired_on==None:
            token.expired_on = dt.datetime.now()
            token.save()

def json_response(obj):
    return HttpResponse(json.dumps(obj), mimetype="application/json")


import os
import fcntl

class DjangoLock:
    #bad lock
    def __init__(self, filename):
        self.filename = filename
    def acquire(self):
        try:
            with open(self.filename) as f:
                return False
        except IOError as e:
            log.info("acquire lock %s"%str(e))
            self.f = open(self.filename, 'w')
            return True
    def release(self):
        self.f.close()
        os.remove(self.filename)
    def __del__(self):
        self.release()

def requireTokenLock():
    pass
def releaseTokenLock():
    pass

import os.path
def get(request):
    PATH = os.path.split(os.path.abspath(__file__))[0]
    filename = os.path.join(PATH, "django.lock")
    lock = DjangoLock(filename)
    if lock.acquire():
        try:
            token = getValidToken()
            lock.release()
            return json_response(token.to_dict())
        except Exception,e:
            lock.release()
            log.error("unknown error：%s"%(str(e)))
            return json_response({"msg":"auth server error, wait a minute and try again"})
    else:
        return json_response({"msg":"auth server busy, wait a minute and try again"})
    pass

def limit(request):
    tokenStr = request.REQUEST["access_token"]
    setLimit(tokenStr)
    return json_response("limited")
    pass
def expire(request):
    tokenStr = request.REQUEST["access_token"]
    setExpire(tokenStr)
    return json_response("expired")
    pass
def exclusive(request):
    return json_response("")
    #TODO
    pass

