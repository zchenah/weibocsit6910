#!/usr/bin/env python
# -*- encoding:utf-8 -*-
#author: Chen Zhao

import urllib
import urllib2
import json
import subprocess
import os.path
import logging

log = logging.getLogger(__name__)

GET_TOKEN_URL = "https://api.weibo.com/oauth2/access_token"

self_path = os.path.dirname(os.path.abspath(__file__))
RB_FILE = os.path.join(self_path,"fakeUserSubmit.rb")
PY_FILE = os.path.join(self_path,"fakeUserSubmit.py")
GET_CODE_SCRIPT = PY_FILE
GET_CODE_SCRIPT = RB_FILE

def getCode(user_name, user_pwd, app_key, app_url):
    proc = subprocess.Popen([GET_CODE_SCRIPT,
            app_url, app_key, user_name, user_pwd],
            stdout=subprocess.PIPE)
    urlWithCode = proc.communicate()[0].strip()
    print urlWithCode
    if "code=" in urlWithCode:
        return urlWithCode.split("=")[1]
    print "no code, something wrong.."

def getTokenFromCode(app_key, app_secret, app_url, code):
    if not code:
        return None
    paras = {"client_id":app_key,
             "client_secret":app_secret,
             "grant_type":"authorization_code",
             "redirect_uri":app_url,
             "code":code}
    post_data = "&".join(["%s=%s"%(k, urllib.quote(v)) for k,v in paras.items()])
    try:
        r = urllib2.urlopen(GET_TOKEN_URL, post_data)
        log.info("token result: %s", r)
        return json.load(r)
    except urllib2.HTTPError,e:
        log.error("HTTPError(%d): %s", e.code, e.read())
        
def getTokenJsn(app_key, app_secret, app_url, user_name, user_pwd):
    code = getCode(user_name, user_pwd, app_key, app_url)
    tokenJsn = getTokenFromCode(app_key, app_secret, app_url, code)
    return tokenJsn
    
class Obj(object):
    pass

def main():
    user_name = "cnjswangheng66@yahoo.com.cn"
    user_pwd = "swarmhere"
    app_key = "767052111"
    app_secret = "a0f8b64ad55a3e065f83160025651f38"
    app_url = "http://www.7dim.com"
    
    token = getTokenJsn(app_key, app_secret, app_url, user_name, user_pwd)
    print token

if __name__=='__main__':
    main()
