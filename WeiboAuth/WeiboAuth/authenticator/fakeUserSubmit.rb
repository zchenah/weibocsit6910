#!/usr/bin/env ruby
require "rubygems"
require "watir-webdriver"

redirect_uri = ARGV[0]
client_id = ARGV[1]
username = ARGV[2]
password = ARGV[3]
auth_url = "https://api.weibo.com/oauth2/authorize?"+
	"redirect_uri=%s&response_type=code&client_id=%s&display=mobile" % [redirect_uri, client_id] 

browser = Watir::Browser.new :ff
browser.goto auth_url
browser.text_field(:id, "userId").set username 
browser.text_field(:name, "passwd").set password
browser.link(:class, "btnP").click
CODE_TIMEOUT = 60
wait_seconds = 0
step2_auth_url = "https://api.weibo.com/oauth2/authorize"
while not browser.url().include?("code=")
	sleep(1)
	wait_seconds+=1
	if browser.url()==step2_auth_url then
		browser.link(:class, "btnP").click
	end
	if wait_seconds>CODE_TIMEOUT
		browser.close()
		abort("timeout!")
	end
end
puts browser.url()
browser.close()
