#!/usr/bin/env python
#author: Chen Zhao
from django.contrib import admin
from WeiboAuth.authenticator.models import *

class AccessTokenAdmin(admin.ModelAdmin):
    #readonly_fields = ("created_on",)
    #fields= ('app','user','expires_in', 'created_on','expired_on','limited_on',)
    list_display = ('app','user','expires_in',
            'created_on','expired_on','limited_on','first_used_on')
    pass
class TestingUserAdmin(admin.ModelAdmin):
    #readonly_fields = ("created_on",)
    #fields= ('name','pwd', 'created_on',)
    list_display= ('name','pwd', 'created_on',)
    pass
class WeiboAppAdmin(admin.ModelAdmin):
    list_display= ('name','key','redirect_uri' ,'info')
    fields = ('testing_users','name','key','secret','redirect_uri','info')
    pass
admin.site.register(WeiboApp, WeiboAppAdmin)
admin.site.register(TestingUser, TestingUserAdmin)
admin.site.register(AccessToken, AccessTokenAdmin)


def main():
    pass

if __name__=='__main__':
    main()

